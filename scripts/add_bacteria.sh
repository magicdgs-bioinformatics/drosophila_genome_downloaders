#!/bin/bash

# For Linux and Mac
# Download and append to the cleaned genome some bacterial genomes
if [ -z $1 ]; then echo "Usage: ./${0##*/} <input.fa> (no_rm_temp)"; exit 1; fi

ACCESSION_NUMBERS=("AE017196.1" "CP001391.1" "CP000416.1" "AP011170.1")
GENOME_NAMES=("wMel" "wRi" "Lactobacillus" "Acetobacter")

# Downloading and getting the output name
outname="${1%.*}."
echo "DOWNLOADING GENOMES:"
for ((i = 0; i < ${#ACCESSION_NUMBERS[@]}; i++));
do
	genome=${GENOME_NAMES[i]}
	outname="${outname}${genome}_"
	echo -e "\t${genome} (${ACCESSION_NUMBERS[i]})"
	if [ ! -f ${genome}.fa ]; then
		curl "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=${ACCESSION_NUMBERS[i]}&rettype=fasta" > ${GENOME_NAMES[i]}.fa
	else
		echo -e "\t...Using already downloaded ${genome}.fa"
	fi
done
outname=${outname%_}.fa

# Append the genomes
echo "ADDING GENOMES TO OUTPUT FILE"
cp ${1} $outname
for ((i = 0; i < ${#ACCESSION_NUMBERS[@]}; i++));
# for genome in ${GENOME_NAMES[@]};
do
	genome=${GENOME_NAMES[i]}
	echo -e "\t$genome (${ACCESSION_NUMBERS[i]})"
	echo ">${genome}_${ACCESSION_NUMBERS[i]}" >> $outname
	grep -v "^>" ${genome}.fa >> ${outname}
done

if [ -z $2 ]; then
	echo "REMOVING TEMP FILES"
	for genome in ${GENOME_NAMES[@]};
	do
		rm ${genome}.fa
	done
fi

echo "DONE: ${outname}"

exit 0
