Miscelaneous scripts
====================

## __add_bacteria.sh__

Sometimes is useful to include some bacteria genomes (for example, *Wolbachia*) when mapping to remove
reads that does not come from the reference.

Concretely, it downloads two bacteria genomes ( [*Lactobacillus.fa*](http://www.ncbi.nlm.nih.gov/nuccore/CP000416.1), 
and [*Acetobacter.fa*](http://www.ncbi.nlm.nih.gov/nuccore/AP011170.1) ) and two _Wolbachia_ strains 
([*wMel.fa*](http://www.ncbi.nlm.nih.gov/nuccore/AE017196.1) from _D. melanogaster_ and
[*wRi.fa*](http://www.ncbi.nlm.nih.gov/nuccore/CP001391.1) from _D. simulans_), added them to a diferent genome (first argument).
