*Drosophila* genomes' downloaders
==============================================

Bash scripts for download the _Drosophila_ genomes and clean the sequences header.
Includes scripts for download some bacteria sequences, and add them to the reference genome.

## Motivation

Download and normalize reference genomes is a difficult task, because sometimes they include unplaced
scaffolds or FASTA headers not compatible with other formats (VCF, BAM, etc). Scripts in this repository
try to generate reference genomes for _Drosophila_ species by downloading from known repositories
(FlyBase, NCBI) and then modify the headers to a more human-usable way.

In addition, other helper scripts are present to other purposes: fix annotation files, add bactiria sequences...

## Requirements

- [bioawk](https://github.com/lh3/bioawk) (available from `brew`)
- [seqtk](https://github.com/lh3/seqtk) (available from `brew`)
- [bedtools](http://bedtools.readthedocs.io/en/latest/) (available from `brew`)
- [GNU's md5sum](https://linux.die.net/man/1/md5sum) (available from `brew` as `md5sha1sum`)
- [wget](https://www.gnu.org/software/wget/) (available from `brew`)

## Usage:

```
# Example for D. melanogaster
	./dmel/download_genome.sh linux
	./scripts/add_bacteria.sh dmel6.03-clean.fa
```

### LICENSE

Scripts included in this repository have a [MIT License](http://opensource.org/licenses/MIT).
