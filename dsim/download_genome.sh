#!/bin/bash

# Needs bioawk and seqtk installed in your path

source $(dirname $0)/sources.conf
# TODO: opt-in a new method to have contig names now that is possible
ACC_NAME_DICT=$(dirname $0)/acc.name.contig.map

outname="${OUTPUT_PREFIX}.clean.fa"
filename="${OUTPUT_PREFIX}.fa.gz"
if [ ! -f ${filename} ]; then
	echo "DOWNLOADING GENOME: $filename";
	wget ${GENOME_LINK} -O ${filename}
else
	echo "USING ALREADY DOWNLOADED GENOME: $filename"
fi
MD5_sum=`md5sum ${filename} | awk '{print $1}'`

if [ "$MD5_sum" != "$MD5_CHECK" ]; then
	echo "ERROR: the downloaded file is not correct:" >&2
	echo -e "\tExpected MD5: $MD5_CHECK" >&2
	echo -e "\tActual MD5:   $MD5_sum" >&2
	exit 1
fi

echo "LOADING ACCESSION NAMES AND CLEANING HEADERS"
# TODO: name[2] uses the old-style name (GI number)
# TODO: here we can have optional different name (accession, name, contig), dependending on which format you want it
bioawk -c fastx 'BEGIN {
while (getline < "'"$ACC_NAME_DICT"'")
{
split($0,ft," ");
id=ft[1];
name=ft[2];

nameArr[id]=name;
}
close("'"$ACC_NAME_DICT"'");
}{print ">"nameArr[$name]}{print $seq}' ${filename} > ${outname}_NOSORT

echo -n "SORTING"
# Obtain major scaffolds
echo -n "..."
bioawk -c fastx '{print $0}' ${outname}_NOSORT | grep -e "^2L\t" -e "^2R\t" -e "^3L\t" -e "^3R\t" -e "^4\t" -e "^X\t" -e "^Y\t" | sort | awk '{print ">"$1"\n"$2}' | seqtk seq -l 80 > ${outname}
# Obtain other scaffolds
echo -n "..."
bioawk -c fastx '$1!="2L" && $1!="2R" && $1!="3L" && $1!="3R" && $1!="4" && $1!="X" && $1!="Y" {print $0}' ${outname}_NOSORT | grep -e "^2L" -e "^2R" -e "^3L" -e "^3R" -e "^4" -e "^[A-z]" | sort | awk '{print ">"$1"\n"$2}' | seqtk seq -l 80 >> ${outname}
# Obtain other contigs
echo "..."
bioawk -c fastx '$1!~/2L/ && $1!~/2R/ && $1!~/3L/ && $1!~/3R/ && $1!="4" {print $0}' ${outname}_NOSORT | grep -v "^[A-z]" | sort | awk '{print ">"$1"\n"$2}' | seqtk seq -l 80 >> ${outname}

# TODO: document this behaviour somehow
if [ -z $1 ]; then
	echo "REMOVING TEMP FILES"
	rm $filename ${outname}_NOSORT
fi

echo "DONE: ${outname}"
