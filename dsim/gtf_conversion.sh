#!/bin/bash

if [ -z $2 ]; then
	echo "Usage: $0 <input.gtf> <output.gtf>"
	exit 1
fi

ACC_NAME_DICT=$(dirname $0)/acc.name.contig.map

echo "LOADING CONTIG NAMES AND SUBSTITUTING IN INPUT"
# TODO: id[3] uses the contig name for ID as the reference and name[2] uses the old-style name (GI number)
# TODO: here we can have optional different name (accession, name, contig), dependending on which format you want it
bioawk -c gff 'BEGIN {
while (getline < "'"$ACC_NAME_DICT"'")
{
split($0,ft," ");
id=ft[3];
name=ft[2];

nameArr[id]=name;
}
close("'"$ACC_NAME_DICT"'");
}{OFS="\t"; $1=nameArr[$1]; print $0}' $1 > $2_NO_SORT

echo "SANITIZE (remove redundant records) AND SORTING AS REFERENCE"
# first get the sort order as it should be (stored in the map)
awk '{print $2}' ${ACC_NAME_DICT} > .sort.order
# then sort using normal sorting to remove redundancy and then with bedtools
sort $2_NO_SORT | uniq | bedtools sort -g .sort.order > $2

# TODO: document behaviour
if [ -z $3 ]; then
	echo "REMOVING TEMP FILES"
	rm $2_NO_SORT .sort.order
fi

echo "FINISH"

exit 0
