_Drosophila simulans_
=====================

_D. simulans_ genome of [M252 strain](http://onlinelibrary.wiley.com/doi/10.1111/1755-0998.12297/abstract)
mantained in [NCBI](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=JMCE01) contains headers with lot of
information. Thus, I clean the header to keep the chromosome arms names, and gi number for the unplaced scaffolds.
In addition, the mitochondrial DNA contains both an informative name (mtDNA) and the gi number.

_Fix: previous NCBI naming on the header was containing GI numbers, but now it contains other identifier.
A map to convert between them was added to this repository to maintain compatibility._

## Reference genome

Including bacterial genomes:

* M252 strain, version 1.1 (JMCE01.1)
* Filename: dsimM252.1.1.clean.wMel_wRi_Lactobacillus_Acetobacter.fa
* MD5: 3d549045d7b77d3e733e14d3b2802690

## Scripts

* __download_genome.sh__: it downloads the genome, checking for the md5sum,
cleans the header (with an accession name map) and sorts the major scaffolds
first and the rest to obtain the file _dsimM252.1.1.clean.fa_.
* __gtf_conversion.sh__: it downloads the genome to have a map of original contig
names to new ones, and rename using this map any gtf file to the _dsimM252.1.1.clean.fa_
contig names.
