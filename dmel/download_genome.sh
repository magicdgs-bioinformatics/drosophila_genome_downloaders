#!/bin/bash

# Needs bioawk and seqtk installed in your path

GENOME_LINK="ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r6.03_FB2014_06/fasta/dmel-all-chromosome-r6.03.fasta.gz"
MD5_CHECK="a0580a0a1c254335beb472b86838a56a"

outname="dmel6.03-clean.fa"
filename="${GENOME_LINK##*/}"
if [ ! -f ${filename} ]; then
	echo "DOWNLOADING GENOME: $filename";
	wget ${GENOME_LINK} -O ${filename}
else
	echo "USING ALREADY DOWNLOADED GENOME: $filename"
fi
MD5_sum=`md5sum ${filename} | awk '{print $1}'`

if [ "$MD5_sum" != "$MD5_CHECK" ]; then
	echo "ERROR: the downloaded file is not correct:" >&2
	echo -e "\tExpected MD5: $MD5_CHECK" >&2
	echo -e "\tActual MD5:   $MD5_sum" >&2
	exit 1
fi

echo -n "SORTING"
# Obtain major scaffolds
echo -n "..."
gzcat ${filename} | awk '{print $1}' | bioawk -c fastx '{print $0}' | grep -e "^2L\t" -e "^2R\t" -e "^3L\t" -e "^3R\t" -e "^4\t" -e "^X\t" -e "^Y\t" | sort | awk '{print ">"$1"\n"$2}' | seqtk seq -l 80 > ${outname}
echo "..."
gzcat ${filename} | awk '{print $1}' | bioawk -c fastx '$1!="2L" && $1!="2R" && $1!="3L" && $1!="3R" && $1!="4" && $1!="X" && $1!="Y" {print $0}' | sort | awk '{print ">"$1"\n"$2}' | seqtk seq -l 80 >> ${outname}

# TODO: document this behaviour somehow
if [ -z $1 ]; then
	echo "REMOVING TEMP FILES"
	rm $filename
fi

echo "DONE: ${outname}"
