#!/bin/bash

if [ -z $2 ]; then
	echo "Usage: $0 <input.gtf> <output.gff>"
	exit 1
fi

grep -e "^2L\t" -e "^2R\t" -e "^3L\t" -e "^3R\t" -e "^4\t" -e "^X\t" -e "^Y\t" $1 | bedtools sort > $2
grep -v "^2L\t\|^2R\t\|^3L\t\|^3R\t\|^4\t\|^X\t\|^Y\t" $1 | bedtools sort >> $2

exit 0
