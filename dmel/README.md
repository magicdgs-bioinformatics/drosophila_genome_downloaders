_Drosophila melanogaster_
=========================

Because in the [release 6.03](ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r6.03_FB2014_06/) of
the _D. melanogaster_ genome there is no a pseudo-scaffold with all the unassembled sequences, it is difficult
to use as a reference such a genome in whole-genome sequencing analysis. For that reason, I generate this code
for download the genome and sort the scaffolds in such a way that the chromosome arms are at the beggining of
the file.

## Reference genome

Including bacterial genomes:

* version 6, release 03
* Filename: dmel6.03-clean.wMel_wRi_Lactobacillus_Acetobacter.fa
* MD5: 187fbf37190e036e93c5208ae10b066f

## Scripts:

* __download_genome.sh__: it downloads the genome, checking for the md5sum (you need to  specify if is linux or mac
because the differences in the name of the tool in both operating systems), cleans the header (keeping only the
first part) and sorts the major scaffolds first and the rest to obtain the file _dmel6.03-clean.fa_.
* __gtf_conversion.sh__: it renames the contigs from gff file to match the
reference from the _D. melanogaster_ created previously (_dmel6.03-clean.fa_).
Does not perform any download.
